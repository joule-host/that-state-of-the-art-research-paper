State of the Art Research Paper
===============================

*Welcome!* To help you get familiar with `joule`, this repo 
is meant to imitate modern research paper code repositories. 

Usage
-----

To get started using Joule, simply `joule pull` in 
the `dataset` directory:

```bash
git clone https://gitlab.com/joule-host/that-state-of-the-art-research-paper.git
cd that-state-of-the-art-research-paper/dataset
joule pull
```

About the dataset
-----------------

The dataset you are about to download is the 
[Georgia Tech Face Database](http://www.anefian.com/research/face_reco.htm). 
As per their authors:

> Georgia Tech face database (128MB) contains images of 
> 50 people taken in two or three sessions between
> 06/01/99 and 11/15/99 at the Center for Signal and 
> Image Processing at  Georgia Institute of Technology.
> All people in the database are represented by 15 color JPEG
> images with cluttered background taken at resolution
> 640x480 pixels. The average size of the faces in these
> images is 150x150 pixels. The pictures show frontal
> and/or tilted faces with different facial expressions,
> lighting conditions and scale. Each image is manually
> labeled to determine the position of the face in the image.
> [...]

If you were to download the dataset by yourself, you would have 
to go on the website, download about 5 zip files, unzip them all, 
and move them to your dataset directory.

Here, you get them all in one `joule` command. 
No download script needed.